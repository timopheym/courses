import React from 'react';
import { IndexLink, Link } from 'react-router';
import { Row, Col } from 'react-flexbox-grid/lib';
// import Paper from 'material-ui/Paper';

import './Header.scss';

export const Header = () => (
    <Row center="xs">      
      
        <Col xs>
          <h1>Study is the key.</h1>
          <IndexLink to='/' activeClassName='route--active'>
            Home
          </IndexLink>
          {' · '}
          <Link to='/courses' activeClassName='route--active'>
            Courses
          </Link>
        </Col>
</Row>
)

export default Header
