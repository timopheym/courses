import React from 'react';
import { IndexLink, Link } from 'react-router';
import { Row, Col } from 'react-flexbox-grid/lib';
// import Paper from 'material-ui/Paper';

import './Footer.scss';
export const Footer = () => (
<Row bottom="xs">
  <Col xs={12}>
    <Row center="xs">
      <Col xs={6} style={{position: 'absolute', bottom: '0px'}}>
          <h3> some copylefts </h3>
          <IndexLink to='/'>
            About
          </IndexLink>
          {' · '}
          <Link to='/'>
            Contacts
          </Link>
        </Col>
    </Row>
  </Col>
</Row>
)

export default Footer
