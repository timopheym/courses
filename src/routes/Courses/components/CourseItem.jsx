import React from 'react'
import { Card } from 'antd';

const stylePrice = 
  (price) => price ? 
      <span>
        <b>Price: </b>
        <i>{price}</i>$ 
      </span>
    :
      <b> Free </b>;
const stripHTML = (html) => {
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}
export const CourseItem = (props) => (
<Card title={<p dangerouslySetInnerHTML={{__html: props.name}} title={stripHTML(props.name)}/>} style={{
  width: "230px"
}}> 
  <p style={{
    marginTop: '-10px',
    marginBottom: '5px',
    float: 'right'
  }}>
    	<i> {props.category} </i> 
      | {stylePrice(props.price)}
	</p>
  <p style={{
      clear: 'both',
      textAlign: 'left'
    }} 
     dangerouslySetInnerHTML={{__html: props.description}} />
</Card>
)

CourseItem.propTypes = {
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  category: React.PropTypes.string.isRequired,
  price: React.PropTypes.number.isRequired
}

export default CourseItem
