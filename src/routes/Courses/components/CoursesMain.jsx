import React, { Component, PropTypes } from 'react';

import CoursesList from './CoursesList';
import CoursesFilter from './CoursesFilter';
import { Row, Col} from 'react-flexbox-grid/lib';
import { Spin } from 'antd';

import './courses.less';

const paper_style = {
	padding: "10px"
};

export class CoursesMain extends Component {
  componentWillMount() {    
    this.props.getCoursesFromServer();
  }
  render() {
    const {
      filter,
      setText,
      setCategories,
      setPrice,
      courses,
      isLoading
    } = this.props;

    if (isLoading) {
     return <Spin 
         style={{
          width: '20px',
          height: '20px',
          display: 'block',
          position: 'absolute',
          top: '50%',
          marginTop: '-10px',
          left: '50%',
         }}
       />;
    }
    
    return (
        <Row center="xs">
        	<Col xs={3} style={{
            height: '80vh'
          }}>
          		<CoursesFilter {...filter} 
                setText={setText}  
                setCategories={setCategories} 
                setPrice={setPrice}/>
          </Col>
          <Col xsOffset={1} xs={8} style={{
            overflowY: 'auto'
          }}>
          		<CoursesList courses={courses}/>
        	</Col>
        </Row>
    );
  }
}

CoursesMain.propTypes = {
  courses: React.PropTypes.array.isRequired,
  filter: React.PropTypes.object.isRequired
}

export default CoursesMain;