import React from 'react'
import CourseItem from './CourseItem';
import chunk from 'lodash.chunk';
import { Row, Col} from 'react-flexbox-grid/lib';

export const CoursesList = (props) => (
  <div style={{ 
  	height: '80vh',
  	margin: '0 auto' 
  }} >  
    {props.courses.length ? chunk(
	    	props.courses.map((course, i) => 
	    					<Col key={`course-${i}`} style={{margin: '5px'}}><CourseItem {...course} /></Col>
			), 3 /*number of elements per row*/
	      ).map((row, i) => <Row  around="xs" style={{margin: '5px'}} key={`courses-row-${i}`}> {row} </Row> )
    : "No courses found =("}
  </div>
)

CoursesList.propTypes = {
  courses: React.PropTypes.array.isRequired
}

export default CoursesList
