import React, { Component, PropTypes } from 'react';
import { Input, Select, Slider, InputNumber, Card, Row, Col } from 'antd';
import range from 'lodash.range';
import zipObject from 'lodash.zipobject';

const Search = Input.Search;
const Option = Select.Option;

const propTypes = {  
  price: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  categories: PropTypes.array.isRequired,

  minPrice: PropTypes.number.isRequired,
  maxPrice: PropTypes.number.isRequired,
  potentialCategories: PropTypes.array.isRequired,

  setText: PropTypes.func.isRequired,
  setPrice: PropTypes.func.isRequired,
  setCategories: PropTypes.func.isRequired
};

export const CoursesFilter = (props) => {
  const {
// Values
    text,
    categories,
    price,

// Settings 
    minPrice,
    maxPrice,
    potentialCategories,

// Actions
    setText,
    setPrice,
    setCategories,
  } = props;

  const changeCategory = (name, value) => value ? addCategory(name) : removeCategory(name);
  
  let marks_buffer = range(minPrice, maxPrice, maxPrice/5).map(v => Math.round(v));
  marks_buffer.push(maxPrice);
  const price_marks = zipObject(marks_buffer, marks_buffer);
  const input_mumber_width = 60;
  return (
    <form> 
      <h2> Search </h2>

      <Search 
        style={{
          width: '100%'
        }}
        placeholder="What do you wanna study?" 
        onChange={e => setText(e.target.value)} />
      
      <br />
      <br />

      <h2> Categories </h2>
      <Select
        multiple
        style={{ width: '100%' }}
        placeholder="Please select categories"
        defaultValue={categories}
        onChange={categories => setCategories(categories)}
      >
        { 
          potentialCategories.map(
           (category, i) => <Option key={category}>{category}</Option>
          )
        }
      </Select>
      
      <br />
      <br />
      
      <h2> Price </h2>
      
      <Row>
        <Col span={6}>
          <InputNumber min={minPrice} max={maxPrice} 
            placeholder="From"
            style={{
              width: `${input_mumber_width}px`,
              marginRight: '10px'
            }}
            value={price.min} onChange={(value) => setPrice({min: value, max: price.max})}
          />
        </Col>
        <Col span={12}>

          <Slider range 
                  min={minPrice}
                  max={maxPrice}
                  marks={price_marks}
                  value={[price.min, price.max]}  
                  onChange={(value) => setPrice({min: value[0], max: value[1]})}
                  tipFormatter={(value) => `${value}$`}
                  />    
        </Col>
        <Col span={2}>
          <InputNumber min={minPrice} max={maxPrice} 
            placeholder="To"
            style={{
              width: `${input_mumber_width}px`,
              marginLeft: '10px'
            }}
            value={price.max} onChange={(value) => setPrice({min: price.min, max: value})}
          />

        </Col>
      </Row>

      {(price.min !== 0 || price.max !== 0) &&        
        <a href="#" onClick={() => setPrice({min: 0, max: 0})}> Show only free </a>
      }   
    </form>
  );
};
CoursesFilter.propTypes = propTypes;

export default CoursesFilter;