import { createSelector } from 'reselect'
import * as Immutable from 'immutable';
import uniq from 'lodash.uniq';
// ------------------------------------
// Constants
// ------------------------------------
export const SET_TEXT_FILTER = 'SET_TEXT_FILTER';
export const SET_CATEGORIES_FILTER = 'SET_CATEGORIES_FILTER';
export const SET_PRICE_FILTER = 'SET_PRICE_FILTER';
export const RECIVE_COURSES = 'RECIVE_COURSES';

// ------------------------------------
// Actions
// ------------------------------------
export const setText = (value) => ({
  type    : SET_TEXT_FILTER,
  payload : value
});

export const setPrice = (value) => ({
  type    : SET_PRICE_FILTER,
  payload : value
});

export const setCategories = (categories) => ({
  type: SET_CATEGORIES_FILTER,
  payload: categories
});

export const getCoursesFromServer = () => 
      (dispatch) => 
        fetch('courses.json').then(
          response => response.json(),
          error => console.error(error)
        )
        .then(courses => dispatch(reciveCoursesFromServer(courses)));

export const reciveCoursesFromServer = (courses) => ({
  type: RECIVE_COURSES,
  payload: courses
});

export const actions = {
  setText,
  setCategories,
  setPrice,
  getCoursesFromServer
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const setFilter = (courses_state, property, value) => {
  const filter = courses_state.get('filter').set(property, value);
  return courses_state.set('filter', filter);
} 

// Main reducers logic
const ACTION_HANDLERS = {
  [SET_TEXT_FILTER] : (courses_state, action) => setFilter(courses_state, 'text', action.payload),
  [SET_PRICE_FILTER] : (courses_state, action) => setFilter(courses_state, 'price', action.payload),
  [SET_CATEGORIES_FILTER] : (courses_state, action) => setFilter(courses_state, 'categories', action.payload),
  [RECIVE_COURSES] : (courses_state, action) => {
    courses_state = setFilter(courses_state, 'price', {
      min: getMinByProperty(action.payload, 'price'),
      max: getMaxByProperty(action.payload, 'price')
    })
    courses_state = setFilter(courses_state, 'categories', uniq(action.payload.map((c) => c.category)));
    courses_state = courses_state.set('items', Immutable.fromJS(action.payload)); //To have all nested object immutable
    courses_state = courses_state.set('isLoading', false);

    return courses_state;
  }
 }

// ------------------------------------
// Selectors
// ------------------------------------
const getMaxByProperty = (array, property) => Math.max.apply(Math, array.map((o) => o[property]))
const getMinByProperty = (array, property) => Math.min.apply(Math, array.map((o) => o[property]))

export const getVisibilityFilter = (state) => {
  let filter = state.courses.get('filter').toJS(),
      courses = getCourses(state).toJS();
  if (courses.length) {
    filter.maxPrice = getMaxByProperty(courses, 'price');
    filter.minPrice = getMinByProperty(courses, 'price');
  } else {
    filter.maxPrice = 0;
    filter.minPrice = 100;
  }
  filter.potentialCategories = uniq(courses.map(c => c.category));
  
  return filter;
}
const getCourses = (state) => state.courses.get('items')

// Filters
const contain = (element_1, element_2) => element_1.indexOf(element_2) > -1; 
const hasText = (course, text) => contain(course.get('name'), text) || contain(course.get('description'), text); 

// If no category selected - do not apply categories filter 
const hasOneOfCategories = (course_category, categories) => categories.length ? contain(categories, course_category) : true; 
const inPriceRange = (price, range) => price >= range.min &&  price <= range.max; 

const insertFirstHighlight = (content, text) => {
  let index = -1,
      text_len = text.length;

  if ((index = content.toLowerCase().indexOf(text.toLowerCase())) > -1) {
      return content.substr(0, index) +
              '<span class="highlight">' + 
                content.substr(index, text_len) +
              '</span>' +
             content.substr(index + text_len);  
  }
  return content;
}

const highlightSearchString = (courses, text) => courses.map((course) => {
  if (text.length) {          
    course.name = insertFirstHighlight(course.name, text);
    course.description = insertFirstHighlight(course.description, text);
  }
  return course;
})

// The filter main logic
export const getVisibleCourses = createSelector(
  [ getVisibilityFilter, getCourses ],
  (visibilityFilter, courses) => {
    const filtred_courses = courses
                                  .filter((course) => 
                                    hasText(course, visibilityFilter.text) &&
                                    hasOneOfCategories(course.get('category'), visibilityFilter.categories) &&
                                    inPriceRange(course.get('price'), visibilityFilter.price)
                                  )
                                  .toJS();

    return highlightSearchString(filtred_courses, visibilityFilter.text);
  }
)

// ------------------------------------
// Reducer
// ------------------------------------
const initialState =  Immutable.Map({
  filter: Immutable.Map({
    "text": "",
    "categories": [],
    "price": {
      min: 10,
      max: 100
    }
  }),
  items: Immutable.List([]),
  isLoading: true
})


export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
