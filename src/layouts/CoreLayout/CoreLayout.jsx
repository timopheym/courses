import React from 'react'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import 'antd/dist/antd.less';
import {Grid, Row, Col} from 'react-flexbox-grid/lib';


export const CoreLayout = ({ children }) => (

  <Grid fluid>
  	<Row bottom="xs">
	  <Col xs={12}>
	    <Header />
	    <div style={{
        height: '80vh',
        position: 'relative'
      }}>
	      {children}
    	</div>
      <Footer />
	  </Col>
    </Row>
  </Grid>
)

CoreLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default CoreLayout
