Trial assignment
=== 
  
A publisher of educational material wants to list all available courses on their
website. The courses page is designed to have a filter column and a results
column of equal height, leaving room for the footer. (see sketch:
https://wireframe.cc/reTo8U). All courses should show by default, in rows of
three. Filter or search actions in the left menu should update the results
without any page refresh or ajax call. The page needs to behave responsively
for optimal display on desktop, tablet and mobile devices.
Details:
- Courses have a name, description, category and price
- Categories are 'calculus', 'language' or 'history'
- Courses can be filtered on name/description using one search field
- Courses can be filtered matching any (not all) of the selected categories
- Courses can be filtered by pricerange
- Multiple filters can be applied simultaneously, results should match all of
them
In short:
- Build and style the described courses page
- Use the attached wireframe as a guideline for layout
- Implement client-side search and filter functionality
- User filter elements you see best fit (the wireframe shows only examples)
- Make the page behave responsively in a user-friendly manner
The page should be functional and without errors. If any additional steps are
required to make your code run, please include a short list of instructions.
Include an overview of the different activities that were part of your process
and the approximate time they took.